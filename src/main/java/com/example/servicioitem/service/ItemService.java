package com.example.servicioitem.service;

import com.example.servicioitem.models.Item;

import java.util.List;

public interface ItemService {

    List<Item> findAll();

    Item findById(Long id, Integer cantidad);
}
