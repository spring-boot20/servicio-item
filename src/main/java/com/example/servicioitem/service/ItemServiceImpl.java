package com.example.servicioitem.service;

import com.example.servicioitem.models.Item;
import com.example.servicioitem.models.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private RestTemplate clienteRest;

    @Override
    public List<Item> findAll() {
        List<Producto> productos = Arrays.asList(clienteRest.getForObject("http://localhost:8001/listar", Producto[].class));
        return productos.stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
//        List<Item> items = new ArrayList<>();
//        productos.forEach(p -> {
//            Item item = new Item(p, 1);
//            items.add(item);
//        });
//        return items;
    }

    @Override
    public Item findById(Long id, Integer cantidad) {
        Map<String, String> params = new HashMap<>();
        params.put("id", id.toString());
        Producto producto = clienteRest.getForObject("http://localhost:8001/ver/{id}", Producto.class, params);
        return new Item(producto, cantidad);
    }
}
