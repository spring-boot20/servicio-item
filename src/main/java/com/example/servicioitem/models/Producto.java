package com.example.servicioitem.models;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Producto {
    private Long id;
    private String name;
    private Double price;
    private LocalDateTime createAt;
}
